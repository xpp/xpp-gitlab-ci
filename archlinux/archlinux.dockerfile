FROM archlinux/archlinux:base-devel

RUN pacman -Syyu                        && \
    pacman -S --needed git cmake gcc

RUN cmake --version

