#build cmake
ARG CMAKE_MA=3
ARG CMAKE_MI=20
ARG CMAKE_PA=1
ARG CMAKE_VER=$CMAKE_MA.$CMAKE_MI.$CMAKE_PA
ARG CMAKE_DIR=cmake-$CMAKE_VER
ARG CMAKE_TAR=$CMAKE_DIR.tar.gz
ARG CMAKE_URL=https://cmake.org/files/v$CMAKE_MA.$CMAKE_MI/$CMAKE_TAR

RUN mkdir /tmp/cmake                                        && \
    cd    /tmp/cmake                                        && \
    wget $CMAKE_URL                                         && \
    tar -xzvf $CMAKE_TAR                                    && \
    cd $CMAKE_DIR                                           && \
    ./bootstrap                                             && \
    make -j$(nproc)                                         && \
    make package                                            && \
    cp cmake-$CMAKE_VER-Linux-x86_64.sh /cmake_installer.sh && \
    rm -rf /tmp/cmake

