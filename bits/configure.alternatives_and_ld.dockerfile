RUN ldconfig                                                                            && \
    update-alternatives --install /usr/bin/gcc      gcc      $(which gcc-10     ) 100   && \
    update-alternatives --install /usr/bin/g++      g++      $(which g++-10     ) 100   && \
    update-alternatives --install /usr/bin/gfortran gfortran $(which gfortran-10) 100
