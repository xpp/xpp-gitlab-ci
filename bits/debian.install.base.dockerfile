#base packages
RUN apt-get install -y build-essential     \
                       ccache              \
                       curl                \
                       doxygen             \
                       g++                 \
                       gcc                 \
                       gfortran            \
                       git                 \
                       graphviz            \
                       libboost-all-dev    \
                       libeigen3-dev       \
                       libssl-dev          \
                       make                \
                       openssl             \
                       subversion          \
                       tar                 \
                       wget

