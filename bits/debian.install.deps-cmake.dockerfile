#base packages
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y build-essential     \
                       curl                \
                       libboost-all-dev    \
                       libssl-dev          \
                       make                \
                       openssl             \
                       subversion          \
                       tar                 \
                       wget

