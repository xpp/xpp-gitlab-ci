#base packages
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y autoconf            \
                       build-essential     \
                       bzip2               \
                       curl                \
                       g++                 \
                       g++-multilib        \
                       gcc                 \
                       gcc-multilib        \
                       make                \
                       tar                 \
                       wget                \
                       xz-utils

