RUN apt-get install -y build-essential  \
                       ccache           \
                       curl             \
                       doxygen          \
                       g++              \
                       gcc              \
                       git              \
                       graphviz         \
                       libboost-all-dev \
                       libeigen3-dev    \
                       make             \
                       subversion       \
                       wget

