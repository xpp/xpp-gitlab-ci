#get cmake apt repo (steps taken from https://apt.kitware.com/)
ARG KITWARE_KEY_URL=https://apt.kitware.com/keys/kitware-archive-latest.asc
ARG REPO_URL=https://apt.kitware.com/ubuntu/
RUN apt-get install -y apt-transport-https                             \
                       ca-certificates                                 \
                       gnupg                                           \
                       software-properties-common                      \
                       wget                                         && \
    wget -O - ${KITWARE_KEY_URL} 2>/dev/null                        |  \
        gpg --dearmor -                                             |  \
        tee /etc/apt/trusted.gpg.d/kitware.gpg >/dev/null           && \
    apt-add-repository "deb ${REPO_URL} $(lsb_release -cs) main"    && \
    apt-get update                                                  && \
    apt-get install kitware-archive-keyring                         && \
    rm /etc/apt/trusted.gpg.d/kitware.gpg                           && \
    apt-get install -y cmake

