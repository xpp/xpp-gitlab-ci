#gcc 10
RUN add-apt-repository ppa:ubuntu-toolchain-r/test  && \
    apt-get update                                  && \
    apt install -y g++-10                              \
                   gcc-10                              \
                   gfortran-10

