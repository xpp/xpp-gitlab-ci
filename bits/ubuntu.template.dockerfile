# syntax = barcodetm/dockerfile-plus

FROM ubuntu:$OS_VER as full_image

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update                      && \
    apt-get upgrade -y

INCLUDE+ bits/ubuntu.install.base.dockerfile
INCLUDE+ bits/ubuntu.install.cmake.dockerfile
INCLUDE+ bits/ubuntu.install.gcc.dockerfile

INCLUDE+ bits/configure.alternatives_and_ld.dockerfile
INCLUDE+ bits/base-ci-setup.dockerfile

RUN apt-get clean && cmake --version && gcc --version

################################################################################
FROM scratch
COPY --from=full_image / /

