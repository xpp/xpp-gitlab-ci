# syntax = barcodetm/dockerfile-plus

FROM debian:10

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update                      && \
    apt-get upgrade -y

INCLUDE+ bits/debian.install.deps-cmake.dockerfile
INCLUDE+ bits/build.cmake.dockerfile
