# syntax = barcodetm/dockerfile-plus

ARG HASH_DEBIAN_10_GCC
ARG REGISTRY

FROM $REGISTRY:debian-10-gcc-$HASH_DEBIAN_10_GCC as substep_1

RUN cd /gcc-10/build        && \
    make install            && \
    cd /                    && \
    rm -rf /gcc-10/build

FROM scratch
COPY --from=substep_1 / /


