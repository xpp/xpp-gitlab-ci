# syntax = barcodetm/dockerfile-plus

FROM debian:10

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update                      && \
    apt-get upgrade -y

INCLUDE+ bits/debian.install.deps-gcc-10.dockerfile

#build gcc
# adapted from https://gist.github.com/s3rvac/76ac07f21d5635209accc11fa56bd3fb
#version
ARG VER_GCC=gcc-10.2.0
ARG VER_GMP=gmp-6.2.0
ARG VER_MPFR=mpfr-4.1.0
ARG VER_MPC=mpc-1.2.1
ARG VER_ISL=isl-0.18

RUN mkdir /gcc-10                                                       && \
    cd    /gcc-10                                                       && \
                                                                           \
    wget https://ftp.wrz.de/pub/gnu/gcc/$VER_GCC/$VER_GCC.tar.xz        && \
    wget https://gmplib.org/download/gmp/$VER_GMP.tar.xz                && \
    wget https://ftp.gnu.org/gnu/mpfr/$VER_MPFR.tar.gz                  && \
    wget https://ftp.gnu.org/gnu/mpc/$VER_MPC.tar.gz                    && \
    wget https://gcc.gnu.org/pub/gcc/infrastructure/$VER_ISL.tar.bz2    && \
                                                                           \
    tar xf $VER_GCC.tar.xz                                              && \
    tar xf $VER_GMP.tar.xz                                              && \
    tar xf $VER_MPFR.tar.gz                                             && \
    tar xf $VER_ISL.tar.bz2                                             && \
    tar xf $VER_MPC.tar.gz                                              && \
                                                                           \
    mv $VER_GMP  $VER_GCC/gmp                                           && \
    mv $VER_MPFR $VER_GCC/mpfr                                          && \
    mv $VER_MPC  $VER_GCC/mpc                                           && \
    mv $VER_ISL  $VER_GCC/isl                                           && \
    mv $VER_GCC  build                                                  && \
    cd build                                                            && \
                                                                           \
    ./configure --enable-languages=c,c++,fortran                           \
                --program-suffix=-10                                       \
                --disable-multilib                                         \
                --enable-checking=release

