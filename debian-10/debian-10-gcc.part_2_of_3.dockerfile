# syntax = barcodetm/dockerfile-plus

ARG HASH_DEBIAN_10_GCC_PART_1_OF_3
ARG REGISTRY

FROM $REGISTRY:debian-10-gcc.part_1_of_3-$HASH_DEBIAN_10_GCC_PART_1_OF_3

RUN ls -l /                     && \
    cd    /gcc-10/build         && \
    make -j$(nproc) all-stage1  && \
    make -j$(nproc) all-stage2

