# syntax = barcodetm/dockerfile-plus

ARG HASH_DEBIAN_10_GCC_PART_2_OF_3
ARG REGISTRY

FROM $REGISTRY:debian-10-gcc.part_2_of_3-$HASH_DEBIAN_10_GCC_PART_2_OF_3

RUN ls -l /             && \
    cd    /gcc-10/build && \
    make -j$(nproc)

