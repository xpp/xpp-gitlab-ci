# syntax = barcodetm/dockerfile-plus

ARG HASH_DEBIAN_10_CMAKE
ARG HASH_DEBIAN_10_GCC_INSTALLED
ARG REGISTRY

FROM $REGISTRY:debian-10-cmake-$HASH_DEBIAN_10_CMAKE as cmake_base
FROM $REGISTRY:debian-10-gcc-installed-$HASH_DEBIAN_10_GCC_INSTALLED as gcc_base

FROM gcc_base as full_image

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update                      && \
    apt-get upgrade -y

INCLUDE+ bits/debian.install.base.dockerfile

#cmake
INCLUDE+ bits/debian.install.deps-cmake.dockerfile
COPY --from=cmake_base /cmake_installer.sh /cmake_installer.sh
RUN ./cmake_installer.sh --skip-license

INCLUDE+ bits/configure.alternatives_and_ld.dockerfile
INCLUDE+ bits/base-ci-setup.dockerfile

RUN apt-get clean && cmake --version && gcc --version

################################################################################
FROM scratch
COPY --from=full_image / /

