# syntax = barcodetm/dockerfile-plus

ARG HASH_DEBIAN_SID_SLIM_CMAKE
ARG REGISTRY
FROM $REGISTRY:debian-sid-slim-cmake-$HASH_DEBIAN_SID_SLIM_CMAKE as cmake_base

FROM debian:sid-slim as full_image

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update                      && \
    apt-get upgrade -y

INCLUDE+ bits/debian.install.base.dockerfile

#cmake
INCLUDE+ bits/debian.install.deps-cmake.dockerfile
RUN echo "get cmake from 'xpp:debian-sid-slim-cmake-$HASH_DEBIAN_SID_SLIM_CMAKE'"
COPY --from=cmake_base /cmake_installer.sh /cmake_installer.sh
RUN ./cmake_installer.sh --skip-license

INCLUDE+ bits/configure.alternatives_and_ld.dockerfile
INCLUDE+ bits/base-ci-setup.dockerfile

RUN apt-get clean && cmake --version && gcc --version

################################################################################
FROM scratch
COPY --from=full_image / /

